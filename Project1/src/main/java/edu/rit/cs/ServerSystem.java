
package edu.rit.cs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Class to perform the actions of a server, takes in a file location and
 * listens for incoming packets, in case the packets are found, store them.
 * Else, as for those packets again.
 *
 * @author Aashish Thakur
 * @since 01/29/2020
 */
public class ServerSystem implements Runnable{
    private static int SIZE;
    private static int seq;
    public static byte[] file;
    private static byte[] ackD = new byte[1];
    //chunk static
    private final static int chunk = 65000;
    private static DatagramSocket socket;
    private String fileLoc;
    private static int lastChunk;
    private static InetAddress dest = null;
    private static int port = 0;


    /**
     * Constructor of the class.
     *
     * @param loc           Takes in the file location.
     */
    public ServerSystem(String loc) {

        fileLoc = loc;
        try {
            // Create a new socket.
            socket = new DatagramSocket(63001);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to process the received data, the config contains information like total size,
     * The last chunk size in case there is an uneven chunk distribution.
     *
     * Performs the word count operation and then sends the result back to client.
     */
    private void receiveData(){
        FileOutputStream fos=null;
        try {

            InetAddress hostAdd  = InetAddress.getLocalHost();
            String address = hostAdd.getHostAddress();
            System.out.println("Listening on  "+ address);

            byte[] receive = new byte[65004];
            byte[] receive1 = new byte[8];

            // create data packet
            DatagramPacket packet
                    = new DatagramPacket(receive1, receive1.length);

            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Get the size of the source file.
            SIZE = getInt(receive1, 0);
            lastChunk = getInt(receive1, 4);
            file = new byte[SIZE];

            //Process the real image data.
            processData(receive);

            //Save the file in the location given by the user.
            File f= new File(fileLoc);
            fos = new FileOutputStream(f);
            if (!f.exists()) {
                f.createNewFile();
            }

            fos.write(file);
            fos.flush();
            System.out.println("File written and now performing word count");
            String resLoc = WordCount_Seq_Improved.startService(fileLoc);
            System.out.println("Sending results to client");
            sendResultsToClient(resLoc);



        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {

                if (fos != null)
                {
                    fos.close();
                }
            }
            catch (IOException ioe) {
                System.out.println("Error in closing the Stream");
            }
        }
    }

    /**
     * Convert the bytes to integer.
     *
     * @param b             Byte input
     * @param init          starting position
     * @return              integer value after conversion.
     */
    public static int getInt(byte[] b, int init)
    {
        return   b[init+3] & 0xFF |
                (b[init+2] & 0xFF) << 8 |
                (b[init+1] & 0xFF) << 16 |
                (b[init] & 0xFF) << 24;
    }


    /**
     * Start the thread for server.
     *
     */
    @Override
    public void run() {
            receiveData();
    }

    /**
     * Process the data received and perform word count.
     *
     *
     * @param receive           The bits that have to processed and then stored depending on the sequence
     *                          number.
     */
    private void processData(byte[] receive){
        //Process all the data.
        while (seq<SIZE){

            DatagramPacket packet;
            try {
                packet = new DatagramPacket(receive, receive.length);
                socket.receive(packet);
                dest = packet.getAddress();
                port = packet.getPort();

            } catch (IOException e) {
                e.printStackTrace();
            }

            // Sequence num.
            seq = getInt(receive,0);

            // Range can vary depending on whether the chunk split is even or not.
            int range = (seq == SIZE - lastChunk ? lastChunk : chunk);

            if(range>SIZE){
                range = SIZE;
            }

            for (int i = 4; i <= range + 3; i++) {
                file[seq] = receive[i];
                seq++;
            }

            packet = new DatagramPacket(ackD, ackD.length, dest, port);
            try {
                socket.send(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Read the bytes of file present in the file location and send those bytes
     * to client.
     *
     * @param location              File location
     */
    private static void sendResultsToClient(String location){
        FileInputStream content;

        try {
            content = new FileInputStream(new File(location));
            byte[] source = content.readAllBytes();
            ManageTransfer.sendingThread(source,socket, dest,port,true);

            System.out.println("Sent to clients");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
