package edu.rit.cs;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Main event manager to handle asynchronous communication between
 * publishers and subscribers.
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class EventManager implements Runnable {


    private static ExecutorService threadPool;
    private static Runnable[] collectionOfThreads;
    private ServerSocket serverSocket;
    private Socket mySocket;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private int threadCount;
    private static synchronizeData syncManager;
    public final int clientPort = 59000;
    private String topicID;

    /**
     * Constructor initializes the publishers and subscriber numbers,
     * starts th threadpool and the concurrent object.
     *
     *
     * @param totalThreads  Total number of pub and sub members
     */
    public EventManager(int totalThreads) {
        Thread mainThread = new Thread(this);
        threadCount = totalThreads;
        threadPool = Executors.newFixedThreadPool(threadCount);
        collectionOfThreads = new Runnable[threadCount];
        syncManager = new synchronizeData();
        setupcollectionOfThreads();
        mainThread.start();
        startRedirection();
    }


    /**
     * Setup the threads.
     *
     */
    public void setupcollectionOfThreads() {

        for (int i = 0; i < threadCount; i++) {
            collectionOfThreads[i] = new pseudoManager(63001 + i, this);
        }
    }

    @Override
    public void run() {
        System.out.println("Going on");

        //write in main

        while (true) {
            HashMap<String, publisherAttributes> pubToProperties;
            HashMap<String, subscribeAttributes> subToProperties;
            ArrayList<Topic> topics;
            ArrayList<Event> events;
            Set<String> offlineSubs;


            synchronized (syncManager) {
                //Get the publisher, subscriber properties, along with
                // a list of subscribers currently unavailable.
                pubToProperties
                        = synchronizeData.getPublisherToProperties();
                subToProperties
                        = synchronizeData.getSubscriberToProperties();
                topics = synchronizeData.getTopicToBeAdvertised();
                events = synchronizeData.getEventsPending();
                offlineSubs = syncManager.offlineSubs;
            }

            // New topics to be advertised
            if (!topics.isEmpty()) {
                System.out.println("TOPICS!");
                System.out.println("size " + topics.size());
                handleTopicAdvertisement(pubToProperties, subToProperties, topics);
                System.out.println("size " + topics.size());

            }

            // New events for subscribers
            if (!events.isEmpty()) {
                System.out.println("Events!");
                handleEventsDue(events);
            }

            // Subscribers that are offline.
            if (!offlineSubs.isEmpty()){
                System.out.println("got pending events for subscribers");
                offlineSubs.forEach(sub -> {
                    if(subToProperties.get(sub).isActive){
                        sendPending(sub,subToProperties);

                        }

                });
            }
        }


    }

    /**
     * Redirect to a new port after a client successfully connects for the first time.
     *
     *
     *
     */
    private void startRedirection() {
        try {
            serverSocket = new ServerSocket(9092);
            int threadIndex = 0;
            while (true) {
                Socket clientSocket = serverSocket.accept();

                System.out.println("Redirection starting with client on " + clientSocket.getInetAddress());

                outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
                inputStream = new ObjectInputStream(clientSocket.getInputStream());
                outputStream.writeObject(63001 + threadIndex);
                threadPool.execute(collectionOfThreads[threadIndex]);
                threadIndex = (threadIndex + 1) % threadCount;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param creds        Credential details
     * @param clientSocket Client's socket
     * @return True if operation successful, else false.
     */
    public boolean verifySession(String[] creds, Socket clientSocket) {
        boolean result;
        synchronized (syncManager) {
            result = syncManager.verifySession(creds, clientSocket);

        }
        return result;
    }

    /**
     * List of topics available
     *
     * @return Map of current Topic IDs and their topics
     */

    public HashMap<Integer, Topic> fetchTopics() {
        synchronized (syncManager) {
            return synchronizeData.topicMap;
        }
    }

    /**
     * Starts the event manager
     *
     *
     * @param args main arguments from command line
     */
    public static void mainSS(String[] args) {
        if (args.length==3)
        new EventManager(Integer.parseInt(args[2]));
    }

    /**
     * Add events to be sent to subscribers
     *
     * @param e Event to be shared.
     */

    public void addEvents(Event e) {
        synchronized (syncManager) {
            synchronizeData.eventsPending.add(e);
            System.out.println("Event has been added for sharing");
        }
    }

    /**
     * In case the subscriber is active send the events, else add
     * it to pending events.
     *
     * @param events List of events that have to be sent.
     */
    public void handleEventsDue(ArrayList<Event> events) {

        HashMap<String, ArrayList<Event>> inactiveSubEvents = new HashMap<>();
        int i = 0;
        // Check if the events are due.
        while (!events.isEmpty()) {
            ArrayList<subscribeAttributes> subList;
            Event e;
            String topicName;
            synchronized (syncManager) {
                e = synchronizeData.eventsPending.remove(0);
                topicName = e.getTopic().getName();
                System.out.println("Handling event for topic:" + topicName);
                subList = syncManager.getSubcribersIPForTopic(topicName);
            }

            subscribeAttributes sub = null;
            //List of subscribers per topic.
            while (!subList.isEmpty()) {
                try {
                    sub = subList.remove(0);
                    String ip = sub.ip;
                    startConnection(clientPort, ip);
                    outputStream.writeObject(2);
                    outputStream.writeObject(e);
                    System.out.println((String) inputStream.readObject());

                } catch (IOException | ClassNotFoundException e1) {
                    System.out.println("Failure to connect to client");
                    if (inactiveSubEvents.containsKey(sub.id)) {
                        inactiveSubEvents.get(sub.id).add(e);
                    } else {
                        ArrayList<Event> event = new ArrayList<>();
                        event.add(e);
                        inactiveSubEvents.put(sub.id, event);
                    }

                } finally {
                    try {
                        this.endConnection();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            i++;
        }
        synchronized (syncManager) {
            //In case there are failed attempts, add them to inactive list.
            syncManager.pendingEvents(inactiveSubEvents);
        }
    }

    /**
     * Adds to the topic mapping hash map and pending topics queue.
     *
     * @param t Topic to be advertised.
     */
    public void addTopicsToBeAdvertised(Topic t, String id) {
        synchronized (syncManager) {
            t.setId(synchronizeData.topicID);
            synchronizeData.topicMap.put(t.getId(), t);
            synchronizeData.topicsToBeAdvertised.add(t);
            synchronizeData.topicID++;
            this.topicID = id;
        }
    }


    /**
     * Handle advertisements for incoming topics.
     *
     *
     * @param pubToProperties               List of publishers and their properties.
     * @param subToProperties               List of subscribers and their properties.
     * @param topics                        List of topics.
     */
    public void handleTopicAdvertisement(
            HashMap<String, publisherAttributes> pubToProperties,
            HashMap<String, subscribeAttributes> subToProperties,
            ArrayList<Topic> topics) {
        HashMap<String, ArrayList<Topic>> inactiveSubTopics = new HashMap<>();
        int i = 0;
        while (i < topics.size()) {
            Topic topic;
            synchronized (syncManager) {
                topic = synchronizeData.topicsToBeAdvertised.remove(0);
                System.out.println("Topic " + topic);
            }

            for (String id : pubToProperties.keySet()
            ) {
                if (!id.equals(topicID))
                    writeTopic(pubToProperties.get(id).ip, topic);
                else {
                    System.out.println("Same publisher");
                }
            }
            for (String id : subToProperties.keySet()
            ) {
                if (!subToProperties.get(id).isActive ||
                        (!writeTopic(subToProperties.get(id).ip, topic))) {
                    if (inactiveSubTopics.containsKey(id)) {
                        inactiveSubTopics.get(id).add(topic);
                    } else {
                        ArrayList<Topic> t = new ArrayList<>();
                        t.add(topic);
                        inactiveSubTopics.put(id, t);
                    }
                }
            }
            i++;
        }
        synchronized (syncManager) {
            syncManager.pendingTopics(inactiveSubTopics);
        }
    }

    /**]
     * Add a new subscriber
     *
     * @param subName
     * @param t
     */
    public void addSubscriber(String subName, Topic t) {
        synchronized (syncManager) {
            syncManager.subscribetoTopic(t, subName);
        }
    }

    /**
     *
     * Remove a topic for a subscriber.
     *
     *
     * @param subName           Subcriber's name
     * @param t                 Topic to be removed.
     * @return                  true if successful else false.
     */
    public boolean removeSubscriber(String subName, Topic t) {
        synchronized (syncManager) {
            return syncManager.unsubscribeFromTopic(t, subName);
        }
    }

    /**
     *
     * Returns a list of subcriptions.
     *
     *
     * @param subID             Subscriber's id.
     * @return                  Returns a list of subcriptions.
     */
    public HashMap<String, Topic> returnSubscribersList(String subID) {
        synchronized (syncManager) {
            return syncManager.returnSubscribedTopic(subID);
        }
    }


    /**
     * Unsubscribe from all the subscriptions for user.
     *
     *
     * @param subscriber                Subcriber requesting.
     */
    public void unsubscribeFromAll(String subscriber) {
        synchronized (syncManager) {
            syncManager.unsubscribeFromAllTopics(subscriber);
        }
    }

    /**
     * Write a topic.
     *
     *
     * @param ip                    Subscriber's ip
     * @param topic                 Topic details
     * @return                      True if success
     */
    public boolean writeTopic(String ip, Topic topic) {
        boolean result;
        try {
            startConnection(clientPort, ip);
            outputStream.writeObject(1);
            outputStream.writeObject(topic);
            result = true;
        } catch (IOException e) {
            System.out.println("Failed connection with client on ip " + ip);
            result = false;
        } finally {
            try {
                this.endConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * Write an event.
     *
     *
     * @param ip                    Subscriber's ip
     * @param event                 Event details
     * @return                      True if success
     */
    public boolean writeEvent(String ip, Event event) {
        boolean result;
        try {
            startConnection(clientPort, ip);
            outputStream.writeObject(2);
            outputStream.writeObject(event);
            result = true;
        } catch (IOException e) {
            System.out.println("Failed connection with client on ip " + ip);
            result = false;
            System.exit(1);
        } finally {
            try {
                this.endConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    /**
     * Start a connection
     *
     * @param port              Port number
     * @param ip                IP address.
     */
    public void startConnection(int port, String ip) throws IOException {
//        try {
            mySocket = new Socket(ip, port);
            outputStream = new ObjectOutputStream(mySocket.getOutputStream());
            inputStream = new ObjectInputStream(mySocket.getInputStream());
//        }
    }

    public void endConnection() throws IOException {
        inputStream.close();
        outputStream.close();
        mySocket.close();
    }


    /**
     * send the pending data for a subscriber when he comes back online.
     *
     *
     * @param subName                           Subscriber name
     * @param subscriberToProperties            Properties.
     */
    public void sendPending(String subName , HashMap<String, subscribeAttributes> subscriberToProperties){
        subscribeAttributes sub = subscriberToProperties.get(subName);
        if (sub.pendingEvents.size()>0 ||
                sub.pendingTopics.size()>0){
            int i = 0;
            while (sub.pendingTopics.size()>i){
                synchronized (syncManager){
                    if (writeTopic(subscriberToProperties.get(subName).ip,sub.pendingTopics.get(i))){
                        sub.pendingTopics.remove(i);
                    }
                  i++;
                }
            }
            i=0;
            while (sub.pendingEvents.size()>i){
                if (writeEvent(subscriberToProperties.get(subName).ip,sub.pendingEvents.get(i))){
                    sub.pendingTopics.remove(i);
                }
                i++;
            }
        }
    }

}
