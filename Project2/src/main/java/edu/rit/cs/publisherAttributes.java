package edu.rit.cs;

/**
 *
 *
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class publisherAttributes {
    String ip;
    int portNum;

    /**
     * Make a note of publisher's ip and port number.
     *
     * @param portNum               publisher's port number
     * @param ip                    publisher's ip address
     */
    publisherAttributes(int portNum, String ip){
        this.ip = ip;
        this.portNum = portNum;
    }
}
