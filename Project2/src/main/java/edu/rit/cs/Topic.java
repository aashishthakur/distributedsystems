package edu.rit.cs;

import java.io.Serializable;
import java.util.List;

/**
 *
 *
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class Topic implements Serializable {
	private int id;
	private List<String> keywords;
	private String name;
	public int type;


	public String getName() {
		return name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
