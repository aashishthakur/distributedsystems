package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Error;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.server.MessageContext;
import com.thetransactioncompany.jsonrpc2.server.RequestHandler;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Handles the incoming request packets,
 * breaks down the request parameters in JSON,
 * calls appropriate server methods for processing.
 *
 */
public class JsonHandler {

	 public static class MiniServerHandler implements RequestHandler {
	     // Reports the method names of the handled requests
		public String[] handledRequests() {
		    return new String[]{"online", "offline", "ispeeronline"};
		}

		// Processes the requests
		public JSONRPC2Response process(JSONRPC2Request req, MessageContext ctx) {
			Map<String, Object> myParams = req.getNamedParams();
			Object id = myParams.get("id");
		    if (req.getMethod().equals("online")) {
				Object ip = myParams.get("ip");
				Object port = myParams.get("port");
				int v = Integer.parseInt(String.valueOf(id));
				MiniServer.addPeer(v, ip.toString(), port.toString());
				return new JSONRPC2Response(true, req.getID());
	         } else if (req.getMethod().equals("offline")) {
	         	MiniServer.removePeer((Integer)id);
				return new JSONRPC2Response(true, req.getID());
	         } else if (req.getMethod().equals("ispeeronline")) {
				return new JSONRPC2Response(MiniServer.isPeerOnline(ClientRPC.getIntFromLong(id)), req.getID());
			} else {
		        // Method name not supported
				return new JSONRPC2Response(JSONRPC2Error.METHOD_NOT_FOUND, req.getID());
	         }
	     }
	 }

	/**
	 * Handler for the peers
	 *
	 */
	public static class PeerHandler implements RequestHandler{

		 @Override
		 public String[] handledRequests() {
			 return new String[]{"contact","findClosest","addfile","lookupfile"};
		 }

		 @Override
		 public JSONRPC2Response process(JSONRPC2Request req, MessageContext ctx) {
			 Map<String, Object> myParams = req.getNamedParams();

			 // Contact method is for making initial contact
			 if (req.getMethod().equals("contact")) {

//				 Map<String, Object> myParams = req.getNamedParams();
				 Object id = myParams.get("id");
				 HashMap<String,Object> peerMap =
						 (HashMap<String, Object>) myParams.get("peer");

				 KademliaPeer peer = new KademliaPeer();
				 // Setup the peer and try to insert it in the tree.
				 peer.setNodeID(Integer.parseInt(String.valueOf(peerMap.get("nodeID"))));
				 peer.setIpAddress((String) peerMap.get("ipAddress"));
				 peer.setPort((String) peerMap.get("port"));
				 boolean isAnchor = (boolean)myParams.get("anchor");
				 ServerRPC.connectNewNode(peer,isAnchor);
//				 if (isAnchor){
				 	return new JSONRPC2Response(ServerRPC.myPeer, req.getID());
//				 }

			 }else if (req.getMethod().equals("findClosest")){
			 	// Find the closest peer from the target.
				 int sourceID = ClientRPC.getIntFromLong(myParams.get("myid"));
				 PeerInfo peerInfo =
						 ServerRPC.rt.findClosest(ClientRPC.getIntFromLong(myParams.get("id")),myParams);
				 if (!routingTable.peerInfoHashMap.containsKey(sourceID)){
				 KademliaPeer peer = new KademliaPeer();
				 peer.setNodeID(sourceID);
				 peer.setIpAddress((String)myParams.get("myip"));
				 peer.setPort((String)myParams.get("myPort"));
				 ServerRPC.connectNewNode(peer,false);
				 }
				 return new JSONRPC2Response(peerInfo, req.getID());

			 }else if (req.getMethod().equals("addfile")){
			 	// Add a new file
			 	int fileID = ClientRPC.getIntFromLong(myParams.get("fileID"));
			 	String data = (String) myParams.get("data");
				 System.out.println("Got req "+ fileID);
				 int future = ServerRPC.addFile(fileID,data);
				 if (future!= ServerRPC.myPeer.getNodeID()){
					 try {
						 URL serverURL = new URL("http://" + routingTable.peerInfoHashMap.get(future).getIpAddress()
								 + ":" + routingTable.peerInfoHashMap.get(future).getPort());
						 System.out.println("send to "+ serverURL.toString());
						 ClientRPC.formRequest(serverURL,"addfile",myParams);
					 } catch (MalformedURLException e) {
						 e.printStackTrace();
					 }
				 }
//
				 return new JSONRPC2Response(true, req.getID());
//				}

			 }else if (req.getMethod().equals("lookupfile")){
				 // Check if file present, if not pass along to best peer
				 int fileID = ClientRPC.getIntFromLong(myParams.get("fileID"));
				 String res=null;
				 System.out.println("Got req to lookup "+ fileID);

				 int future = ServerRPC.findFile(fileID);
				 if (ServerRPC.fileData.containsKey(fileID))
				 	res = ServerRPC.fileData.get(fileID);
				 else if (future == ServerRPC.myPeer.getNodeID()){
					 res = ServerRPC.fileData.get(future);
				 }
				 else {
				 JSONRPC2Response response = null;

				 if (future!= ServerRPC.myPeer.getNodeID()){
					 try {
						 URL serverURL = new URL("http://" + routingTable.peerInfoHashMap.get(future).getIpAddress()
								 + ":" + routingTable.peerInfoHashMap.get(future).getPort());
						 System.out.println("send to "+ serverURL.toString());
						 response = ClientRPC.formRequest(serverURL,"lookupfile",myParams);
						  res = (String) response.getResult();
					 } catch (MalformedURLException e) {
						 e.printStackTrace();
					 }
				 }}
//
				 return new JSONRPC2Response(res, req.getID());
//				}

			 }
			 return null;
		 }
	 }
}
