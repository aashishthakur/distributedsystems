//package edu.rit.cs;
//
//
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.net.Socket;
//import java.util.HashMap;
//import java.util.Scanner;
//
///**
// * @author Aashish Thakur(at1948@rit.edu)
// * @version 1.0
// */
//public class PubSubbk implements Publisher, Subscriber, Runnable {
//
//    private String ip;
//    private ObjectOutputStream outputStream;
//    private ObjectInputStream inputStream;
//    private ObjectInputStream mapInputStream;
//    private Socket mySocket;
//    private int newServerPort;
//
//    public enum pubOptions {PUBLISH, ADVERTISE}
//
//    ;
//
//    public enum subOptions {SUBSCRIBE, UNSUBSCRIBE, SUBLIST, EXIT}
//
//    ;
//    //    static ArrayList<Topic> topicMap = new
////            ArrayList<>();
//    public static HashMap<Integer, Topic> topicMap = new
//            HashMap<>();
//
//    static String type;
//
//    public PubSubbk(String ip) {
//        this.ip = ip;
//
//        try {
//            startConnection(9092);
//            newServerPort = (int) inputStream.readObject();
//
//            System.out.println(newServerPort);
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                endConnection();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }
//
//    public void verifyCredentials() {
//        try {
//            Scanner sc = new Scanner(System.in);
//            startConnection(newServerPort);
//            while (true) {
//                System.out.println("Please enter user id");
//
//                String userID = sc.next();
//                System.out.println("Please enter password");
//                String password = sc.next();
//                System.out.println("Please enter 'p' for publisher or 's' for subscriber");
//                type = sc.next();
//                // 01: Login verification,
//                type = type.toLowerCase();
//                if (!(type.equals("p") || type.equals("s"))) {
//                    System.err.println("invalid type");
//                    continue;
//                }
//                String creds[] = {userID, password, type};
//                outputStream.writeObject(1);
//                outputStream.writeObject(creds);
//                if ((boolean) inputStream.readObject()) {
//
//                    System.out.println("Login successful");
//                    endConnection();
//                    break;
//                } else {
//                    System.err.println("Login failure");
//
//                }
//            }
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void startConnection(int port) {
//        try {
//            mySocket = new Socket(ip, port);
//            outputStream = new ObjectOutputStream(mySocket.getOutputStream());
//            inputStream = new ObjectInputStream(mySocket.getInputStream());
//        } catch (IOException e) {
//            System.out.println("Connection issue");
//            System.exit(1);
//        }
//
////        System.out.println("Successful redirection");
//
//    }
//
//    public void endConnection() throws IOException {
//        inputStream.close();
//        outputStream.close();
//
//        mySocket.close();
//    }
//
//    @Override
//    public void subscribe(Topic topic) {
//        // TODO Auto-generated method stub
//
//    }
//
//    @Override
//    public void subscribe(String keyword) {
//        // TODO Auto-generated method stub
//
//    }
//
//    @Override
//    public void unsubscribe(Topic topic) {
//        // TODO Auto-generated method stub
//    }
//
//    @Override
//    public void unsubscribe() {
//        // TODO Auto-generated method stub
//
//    }
//
//    @Override
//    public void listSubscribedTopics() {
//        // TODO Auto-generated method stub
//
//    }
//
//    @Override
//    public void publish(Event event) {
//        // TODO Auto-generated method stub
//        try {
//            startConnection(newServerPort);
//
//            outputStream.writeObject(3);
//            outputStream.writeObject(event);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                endConnection();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }
//
//    @Override
//    public void advertise(Topic newTopic) {
//        // TODO Auto-generated method stub
//        try {
//            startConnection(newServerPort);
//            outputStream.writeObject(4);
//            outputStream.writeObject(newTopic);
//            System.out.println(inputStream.readObject());
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                endConnection();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//
//
//    @Override
//    public void run() {
//
//    }
//
//
//    public static void main(String[] args) {
//
//        if (args.length == 0 || args.length > 1) {
//            System.err.println("Invalid Inputs, please pass the server address");
//            System.exit(1);
//
//        }
//
//        String server_address = args[0];
//
//
//        PubSubbk pubSub = new PubSubbk(server_address);
//        pubSub.verifyCredentials();
//
//        if (type.equals("s")) {
//            subcriberGUI(pubSub);
//        } else publisherGUI(pubSub);
//
//    }
//
//
//    public static void subcriberGUI(PubSubbk pubSub) {
//
//        while (true) {
//            int i = 1;
//            Scanner sc = new Scanner(System.in);
//            System.out.println("Enter");
//            for (subOptions sub : subOptions.values()) {
//
//                System.out.printf(" %d for %s\n", i, sub);
//                i++;
//            }
//            int index = sc.nextInt();
//
//            switch (subOptions.values()[index - 1]) {
//                case SUBSCRIBE:
//                    System.out.println("Yes");
//            }
//        }
//    }
//
//    public static void publisherGUI(PubSubbk pubSub) {
//        while (true) {
//            int i = 1;
//            Scanner sc = new Scanner(System.in);
//            System.out.println("Enter");
//            for (pubOptions pub : pubOptions.values()) {
//
//                System.out.printf(" %d for %s\n", i, pub);
//                i++;
//            }
//            int index = sc.nextInt();
//            if (index == 0) {
//                System.err.println("Invalid choice");
//                continue;
//            }
//            switch (pubOptions.values()[index - 1]) {
//                case PUBLISH:
//                    System.out.println("Please select a topic:\n");
//                    Event e = null;
//                    pubSub.fetchTopics();
//                    pubSub.printTopics();
//                    int choice = sc.nextInt();
////                    for (int j = 0; i< topicMap.size();j++) {
////
////
////                        if (topicMap.get(j).getId() == choice) {
////                            System.out.println("Enter Title");
////                            String title = sc.next();
////                            System.out.println("Enter Content");
////                            String content = sc.next();
////                            e = new Event();
////                            e.setTitle(title);
////                            e.setTopic(topicMap.get(j));
////                            e.setContent(content);
////                            pubSub.publish(e);
////                        }
////                    }
//
//
//                    if (topicMap.containsKey(choice)) {
//                        System.out.println("Enter Title");
//                        String title = sc.next();
//                        System.out.println("Enter Content");
//                        String content = sc.next();
//                        e = new Event();
//                        e.setTitle(title);
//                        e.setTopic(topicMap.get(choice));
//                        e.setContent(content);
//                        pubSub.publish(e);
//                    } else
////                    if (e==null) {
//                        System.out.println("Invalid choice");
////                    }
//
//                    break;
//
//                case ADVERTISE:
//                    while (true) {
//                        System.out.println("Enter Topic name");
//                        String topicName = sc.next();
//                        pubSub.fetchTopics();
//                        if (topicMap.containsValue(topicName)) {
//                            Topic t = new Topic();
//                            t.setName(topicName);
//                            pubSub.advertise(t);
//                            break;
//                        }
//                    }
//                    break;
//                default:
//                    System.out.println("Invalid choice");
//                    break;
//
//            }
//        }
//    }
//
//
//    public void fetchTopics() {
//        try {
//            startConnection(newServerPort);
//            outputStream.writeObject(2);
//            mapInputStream = new ObjectInputStream(inputStream);
////            topicMap = (ArrayList<Topic>) mapInputStream.readObject();
//            topicMap = (HashMap<Integer, Topic>) mapInputStream.readObject();
//            mapInputStream.close();
//
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                endConnection();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public void printTopics() {
//
//        if (topicMap.isEmpty()) {
//            System.out.println("No topics available right now.");
//        } else {
////            for (Topic topic : topicMap) {
////                System.out.printf("%d:%s", topic.getId(), topic);
////            }
//            for (int topicID : topicMap.keySet()) {
//                System.out.printf("%d:%s", topicID, topicMap.get(topicID));
//            }
//        }
//
//    }
//
//
//}
