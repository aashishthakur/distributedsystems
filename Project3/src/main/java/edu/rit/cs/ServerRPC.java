package edu.rit.cs;

//The JSON-RPC 2.0 Base classes that define the
//JSON-RPC 2.0 protocol messages
import com.thetransactioncompany.jsonrpc2.*;
//The JSON-RPC 2.0 server framework package
import com.thetransactioncompany.jsonrpc2.server.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


public class ServerRPC implements Runnable{
    /**
     * The port that the server listens on.
     */
    private static final int port = Integer.parseInt(Config.SERVER_PORT_VAL);
    static routingTable rt ;
    public static KademliaPeer myPeer;

    public static HashMap<Integer, String> fileData = new HashMap<>();

    public ServerRPC(KademliaPeer myPeer) {
        ServerRPC.myPeer = myPeer;
        rt = new routingTable(myPeer);
        rt.insert(myPeer);
//        peerIn

    }


    /**
     * Add a file to the file mappings
     *
     *
     * @param fileID            Id of the file
     * @param data               Data entered
     * @return                   Returns the id where it could be/is stored
     */
    public static int addFile(int fileID, String data){
        int min = Integer.MAX_VALUE;
        for (PeerInfo pi : routingTable.peerInfoHashMap.values()){
            min = tree.xorComp(Integer.parseInt(pi.getId()),min,fileID);
        }
        if (min==myPeer.getNodeID()){
            fileData.put(min,data);
            for (int id : fileData.keySet()){
                System.out.println(id+" : "+ fileData.get(id));
            }
        }
        return min;
    }

    /**
     * Check if we can find the file in the hashmap
     *
     * @param fileID            Id for the lookup
     * @return                  Closest node
     */
    public static int findFile(int fileID){
        int min = Integer.MAX_VALUE;
        for (PeerInfo pi : routingTable.peerInfoHashMap.values()){
            min = tree.xorComp(Integer.parseInt(pi.getId()),min,fileID);
        }
        return min;
    }


    public static boolean connectNewNode(KademliaPeer peer, boolean isAnchor){
        boolean res =  rt.insert(peer);
        if (res){
            System.out.println("Success");
            printRT();
        }
        return res;
    }

    public static void printRT(){
        System.out.println(" "+KademliaPeer.getBinary(
                myPeer.getNodeID())+ " : "+myPeer.getNodeID());
        for (PeerInfo pi: routingTable.peerInfoHashMap.values()){
            if (Integer.parseInt(pi.getId())!=myPeer.getNodeID())
            System.out.println(" "+KademliaPeer.getBinary(
                    Integer.parseInt(pi.getId()))+ " : "+pi.getId());
        }
    }

    @Override
    public void run() {
        try (ServerSocket listener = new ServerSocket(port)){
            System.out.println("The server is running.");
            while (true) {
                new Handler(listener.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * A handler thread class.  Handlers are spawned from the listening
     * loop and are responsible for a dealing with a single client
     * and broadcasting its messages.
     */
    private static class Handler extends Thread {
        private String name;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private Dispatcher dispatcher;

        /**
         * Constructs a handler thread, squirreling away the socket.
         * All the interesting work is done in the run method.
         */
        public Handler(Socket socket) {
            this.socket = socket;

            // Create a new JSON-RPC 2.0 request dispatcher
            this.dispatcher =  new Dispatcher();

            // Register the "PeerHandler" handler with it
            dispatcher.register(new JsonHandler.PeerHandler());


        }

        /**
         * Services this thread's client by repeatedly requesting a
         * screen name until a unique one has been submitted, then
         * acknowledges the name and registers the output stream for
         * the client in a global set, then repeatedly gets inputs and
         * broadcasts them.
         */
        public void run() {
            try {
                // Create character streams for the socket.
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // read request
                String line;
                line = in.readLine();
                //System.out.println(line);
                StringBuilder raw = new StringBuilder();
                raw.append("" + line);
                boolean isPost = line.startsWith("POST");
                int contentLength = 0;
                while (!(line = in.readLine()).equals("")) {
                    //System.out.println(line);
                    raw.append('\n' + line);
                    if (isPost) {
                        final String contentHeader = "Content-Length: ";
                        if (line.startsWith(contentHeader)) {
                            contentLength = Integer.parseInt(line.substring(contentHeader.length()));
                        }
                    }
                }
                StringBuilder body = new StringBuilder();
                if (isPost) {
                    int c = 0;
                    for (int i = 0; i < contentLength; i++) {
                        c = in.read();
                        body.append((char) c);
                    }
                }

//                System.out.println(body.toString());
                JSONRPC2Request request = JSONRPC2Request.parse(body.toString());
                JSONRPC2Response resp = dispatcher.process(request, null);


                // send response
                out.write("HTTP/1.1 200 OK\r\n");
                out.write("Content-Type: application/json\r\n");
                out.write("\r\n");
                out.write(resp.toJSONString());
                // do not in.close();
                out.flush();
                out.close();
                socket.close();
            } catch (IOException e) {
                System.out.println(e);
            } catch (JSONRPC2ParseException e) {
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }



}