<h1>Methods explanation </h1>

<h2>Publisher:</h2>

<p>1.) eventsUpdate(): Updates any new events that are received from other publishers.
<p>2.) checkForUpdates(): Runs on a separate thread checking if there are any updates to be sent to the 
subscribers.


<h2>Subscriber:</h2>
<p>1.) pingStatus(): Sends the subscriber ID and current time every 5 seconds, if no updates received for 15 seconds, the subscriber is  no longer active.
<p>2.) Subscribe(): Subscription updates from the list of events


<h2>EventManager:</h2>
<p>1.) getEvents(): Converts the eventList into bytes and then provides it to Pub/Sub Login to send to those logging in.
<p>2.) verify(): Subscriber to object Hash-map contains password for a given ID, use this to verify login details.
<p>3.) updateConnection(): If subscriber exits, update this information.
<p>4.) deadOrAliveUpdate(): Checks if the time difference between current time and subscribers time is less than 15 seconds or not, if not then make subscriber inactive.
<p>5.) modifySubscribers(): For new subscribers to existing events append to the subscriber ID list.
<p>6.) updateTable() and checkActivity(): Updates a table(ArrayList) of inactive subscribers, check activity checks in case they are active again, if so, remove from the list and move to Queue. 



<h3>Instructions to run<h3>
Start & end
==============
sudo docker-compose -f docker-compose-p2.yml -p Project2 up -d --build --scale pub=2 --scale sub=2
sudo docker-compose -f docker-compose-p2.yml -p Project2 down

ATTACH
==============
sudo docker exec -it project2_eventmanager_1 bash ./start.sh 1 1 <number of pub+sub>

sudo docker exec -it project2_publisher_1 bash
    ./start.sh 2

sudo docker exec -it project2_publisher_2 bash
    ./start.sh 2

sudo docker exec -it project2_subscriber_1 bash

sudo docker exec -it project2_subscriber_2 bash