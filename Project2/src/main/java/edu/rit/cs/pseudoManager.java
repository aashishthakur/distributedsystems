package edu.rit.cs;

import javax.sound.midi.Soundbank;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * Pseudo event manager to handle asynchronous communication between
 * publishers and subscribers.
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class pseudoManager implements Runnable{
    private int portNum;
    private ServerSocket changedServerSocket;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private EventManager eManager;


    pseudoManager(int portNum, EventManager eManager){
        this.portNum = portNum;
        this.eManager = eManager;
        try {
            changedServerSocket = new ServerSocket(portNum);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Socket clientSocket = changedServerSocket.accept();
                System.out.println("Redirection successful, new port is " + changedServerSocket.getLocalPort());
                outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
                inputStream = new ObjectInputStream(clientSocket.getInputStream());

                int choice = (int) inputStream.readObject();
                System.out.println("User chose: " + choice);
                switch (choice) {
                    // Verify
                    case 1:
                        System.out.println("Verifying credentials");
                        String[] creds = (String[]) inputStream.readObject();
                        boolean result = eManager.verifySession(creds,clientSocket);
                        outputStream.writeObject(result);
                        break;
                    // Get topic list
                    case 2:
                        System.out.println("Fetching all the topics");
                        ObjectOutputStream mapOutputStream = new ObjectOutputStream(outputStream);
                        mapOutputStream.writeObject(eManager.fetchTopics());
                        mapOutputStream.flush();
                        System.out.println("done");
                        break;
                    // Publish to subscribers.
                    case 3:
                        System.out.println(("Publishing events"));
                        eManager.addEvents((Event) inputStream.readObject());
                        break;
                    // Advertising to all
                    case 4:
                        String id = (String) inputStream.readObject();
                        Topic incomingTopic = (Topic) inputStream.readObject();
                        System.out.println("Advertising new topic "+ incomingTopic);
                        eManager.addTopicsToBeAdvertised(incomingTopic,id);

                        outputStream.writeObject("Topic "+ incomingTopic+" advertised successfully.");
                        break;
                    // Subscribe to a topic
                    case 5:
                        String subID = (String) inputStream.readObject();
                        incomingTopic = (Topic) inputStream.readObject();
                        eManager.addSubscriber(subID,incomingTopic);
                        System.out.println("Subscribed to topic"+ incomingTopic+ " for subscriber "+ subID);
                        break;
                    // Unsubscribe from topic
                    case 6:
                        subID = (String) inputStream.readObject();
                        incomingTopic = (Topic) inputStream.readObject();
                        eManager.removeSubscriber(subID,incomingTopic);
                        System.out.println("Unsubscribed from topic"+ incomingTopic+ " for subscriber "+ subID);
                        break;
                    // Unsubscribe from all topics
                    case 7:
                        subID = (String) inputStream.readObject();

                        eManager.unsubscribeFromAll(subID);
                        System.out.println("Unsubscribed from all topics for subscriber "+ subID);
                        break;
                    // Get all the subscriptions
                    case 8:
                        subID = (String) inputStream.readObject();
                        System.out.println("Fetching all the subscriptions for "+subID);
                        outputStream.writeObject(eManager.returnSubscribersList(subID));
                        break;

                }
            }
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
