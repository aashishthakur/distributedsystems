package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class contains all the information about a given
 * node, it contains information like port no, ip, node no,
 * It also acts as both client and server to form a peer
 * to peer architecture.
 */
public class KademliaPeer implements Serializable {
    enum STATUS {
        ONLINE, OFFLINE;
    }

    private int nodeID;
    private String ipAddress;
    private String port;
    private int myAnchor;

    public void setMyAnchor(int myAnchor) {
        this.myAnchor = myAnchor;
    }

//    private boolean isAnchor;

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getPort() {
        return port;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public int getNodeID() {
        return nodeID;
    }

    /*
    Get the binary of a number
     */
    public static String getBinary(int number){
        String res = Integer.toBinaryString(number);
        if (res.length()<Config.entries){
            int len = res.length();
            for (int i =0; i< Config.entries-len; i++){
                res= "0" + res;
            }
        }
        return res;
    }

    /**
     *
     * The main function
     *
     * @param args          Command line arguments include type of machine, client
     *                      or peer,node num and ip address.
     */
    public static void main(String[] args) {
        try{
            if (args.length==0){
                usage();
            }

            // Get ‘our sending address (perhaps for filtering later...)
            KademliaPeer peer = new KademliaPeer();
            InetAddress localhost = InetAddress.getLocalHost();

            peer.setIpAddress(localhost.getHostAddress().trim());
            System.out.println("IP: "+ localhost.getHostAddress()+ " "+ localhost.hashCode());
            int hash = Math.abs(localhost.hashCode()) % Config.total;
            if (args[0].equals("p")){
                Thread client= null;
                peer.setNodeID(Integer.parseInt(args[1]));
                System.out.println("I'm node "+args[1]);
                peer.setPort(Config.SERVER_PORT_VAL);

                // Starting Server
                Thread sender=new Thread(new ServerRPC(peer));
                sender.start();

                if (args.length==3){
                    System.out.println("Start Client");
                    client=new Thread(new ClientRPC(args[2].trim(),peer,0));
                }else {
                    System.out.println("Start Client");
                    client=new Thread(new ClientRPC("",peer,0));

                }
                client.start();

            while(true);
            }
            else {
                while (true){
                    // UI for the client.
                    if (args.length==2){
                        System.out.println("Select 1 for inserting file");
                        System.out.println("Select 2 for lookup file");
                        Scanner sc = new Scanner(System.in);

                        int option = sc.nextInt();
                        switch (option){
                            case 1:
                                System.out.println("Enter data:");

                                String data = sc.next();
                                sendData(data,args[1],hash, "addfile");
                                System.out.println("Use ID: "+ hash);
                                break;
                            case 2:
                                System.out.println("Enter ID");
                                int id = sc.nextInt();
                                JSONRPC2Response response = sendData("",args[1],id, "lookupfile");

//                                JSONRPC2Response
                                System.out.println((String) response.getResult());
                                break;
                        }
                    }
                }
            }
        }catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public static void usage(){
        System.err.println("Error starting the program");
        System.exit(1);
    }


    /**
     * Generic send data function
     *  @param data              Data to be sent
     * @param ipAddress         Destination IP
     * @param hash              ID
     * @param method            Method who will process it
     */
    public static JSONRPC2Response sendData(String data, String ipAddress, int hash, String method){

        try {
            URL serverURL = new URL("http://" + ipAddress + ":" + Config.SERVER_PORT_VAL);
            Map<String, Object> myParams = new HashMap<>();
            myParams.put("fileID",hash);
            myParams.put("data",data);

            return ClientRPC.formRequest(serverURL,method,myParams);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
