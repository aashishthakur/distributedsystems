package edu.rit.cs;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Trie data structure is used here in order to maintain nodes in
 * a routing table for a peer
 *
 *
 */
public class tree {
    static String rootVal;
    static private Node root;
    static int rootNo;
    static int oldNode;
    public static void startTree(KademliaPeer peer){
        rootNo = peer.getNodeID();
        rootVal = KademliaPeer.getBinary(rootNo);
        root = new Node();
        buildSkeleton();
    }

    /**
     * If possible, insert the node in the data structure.
     * These are the binary nodes of the tree not leaf.
     *
     * @param number  Insert the node number for the peer.
     */
    private static void insert(String number) {
        Node current = root;

        for (int i = 0; i < number.length(); i++) {
            current = current.getChildren().computeIfAbsent(number.charAt(i), c -> new Node());
        }
        current.setEnd(true);
    }

    public static void main(String[] args) {
        KademliaPeer p =  new KademliaPeer();
        p.setNodeID(13);
        startTree(p);

        tree.setNode(13);
        tree.setNode(4);
        tree.setNode(5);
        tree.setNode(1);
        tree.setNode(2);
        tree.setNode(10);
        tree.setNode(11);

//        System.out.println(tree.find("1111"));

    }


    /**
     * Build the initial skeleton for the node,
     * only the skeleton is built here, no values are inserted.
     *
     */
    public static void buildSkeleton(){
        insert(rootVal);
        int i = 1;
        int len = rootVal.length();
        StringBuilder val = new StringBuilder(rootVal);
        while (i <= 4){
            int value = (int) rootVal.charAt(len - i)-48==0?1:0;
            if (val.length()!=0) {
                val.setCharAt(val.length() - 1, (char) (value + 48));
            }else val.append( (char) (value + 48));
            insert(String.valueOf(val) );
            val.delete(len-i,len-i+1);
            i++;
        }
    }


    /**
     * Set the values of the nodes, these are the actual values to be
     * passed in the tree.
     *
     * @param nodeNum           Node number
     * @return                  True if success, else false.
     */
    static boolean setNode(int nodeNum) {
        Node current = root;
        String word = KademliaPeer.getBinary(nodeNum);
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            Node node = current.getChildren().get(ch);
            if (node == null && current.isEnd() || (i==word.length()-1 && node.isEnd())) {

                if (i==word.length()-1 && node != null && node.isEnd()){
                    current = node;
                }
                int content = current.getContent();
                if (content == -1) {
                    current.setContent(nodeNum);
                    break;
                }else {
                    if (xorComp(content,nodeNum)){
                        oldNode = content;
                        current.setContent(nodeNum);
                        break;
                    } else return false;
                }
            }
            current = node;
        }
        return true;
    }

    /**
     * XOR comparison between two given values
     *
     *
     * @param oldVal            Old value
     * @param newVal            New Value
     * @return                  Returns the value with minimum XOR distance.
     */
    public static boolean xorComp(int oldVal, int newVal){

        if ((newVal^rootNo) == Integer.min(rootNo^oldVal, rootNo^newVal)){
            return true;
        }
        return false;
    }

    /*
    Same as above

     */
    public static int xorComp(int oldVal, int newVal, int root){
        int minV = Integer.min(root^oldVal, root^newVal)^root;
        if ((newVal^root) == Integer.min(root^oldVal, root^newVal)){
            return newVal;
        }
        return oldVal;
    }


    public static void setOldNode(int oldNode) {
        tree.oldNode = oldNode;
    }



}

/**
 *
 * Inside structure for the trie.
 *
 */
class Node {
//    ConcurrentHashMap
    private HashMap<Character, Node> children= new HashMap<>();;
    private int content=-1;
    private boolean end;

    HashMap<Character, Node> getChildren() {
        return children;
    }

    boolean isEnd() {
        return end;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }

    void setEnd(boolean endOfWord) {
        this.end = endOfWord;
    }


}