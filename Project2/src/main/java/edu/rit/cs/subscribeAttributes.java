package edu.rit.cs;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * All the subcriber attributes like port, ip, list of pending events and topics, etc.
 *
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class subscribeAttributes {
    String id;
    boolean isActive = false;
    String ip;
    int portNum;

    ArrayList<Event> pendingEvents = new ArrayList<>();
    ArrayList<Topic> pendingTopics= new ArrayList<>();
    //    ArrayList<Topic> subscribedTo = new ArrayList<>();
    HashMap<String,Topic> subscribedTo = new HashMap<>();

    subscribeAttributes(int portNum, String ip, String id){
        this.ip = ip;
        this.portNum = portNum;
        this.id = id;
    }

    public void addEvents(ArrayList<Event> events){
        pendingEvents.addAll(events);

    }


    public void addSubscriber(Topic t){
        subscribedTo.put(t.getName(),t);
    }

    public HashMap<String, Topic> getSubscribedTo() {
        return subscribedTo;
    }

    public Topic removeSubscriber(Topic t){
        subscribedTo.remove(t.getName());
        return t;
    }

    public void removeAllSubs(){
        subscribedTo.clear();
    }

    public void addTopics(ArrayList<Topic> topics){
        pendingTopics.addAll(topics);

    }
    public void removeEvents(){
        pendingEvents.remove(0);

    }
    public void removeTopic(){
        pendingTopics.remove(0);
    }




    void turnOff(){
        this.isActive = false;
    }
    void turnOn(){
        this.isActive = true;
    }


}
