package edu.rit.cs;

public class PeerInfo {
	private String ipAddress;
	private String port;
	private String id;
	public PeerInfo(String ipAddress, String port, String id){
		this.ipAddress = ipAddress;
		this.port = port;
		this.id=id;
	}

	public String getId() {
		return id;
	}

	public String getIpAddress(){ return this.ipAddress; }
	public String getPort(){ return this.port; }
}
