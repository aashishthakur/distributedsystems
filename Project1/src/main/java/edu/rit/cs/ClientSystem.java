package edu.rit.cs;


import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

/**
 * Class to perform the actions of a client, takes in a file location and
 * send packets of the csv file on which word count has to be performed,
 * then receive the results, and print and store them in another file.
 *
 * @author Aashish Thakur
 * @since 01/29/2020
 */
public class ClientSystem implements Runnable {
    //chunk static
    final static int chunk = 65000;
    //Contains the actual file
    private byte[] source;
    //sequence num
    static int seq = 0;
    static int lastChunk = 0;
    private InetAddress ip;
    private FileInputStream content;
    public static byte[] ackD = new byte[1];
    private static DatagramSocket socket;

    public String destination;
    public static String address;

    /**
     * Client's constructor
     *
     * @param destination Destination of the client
     * @param source      Client's file data
     * @param ip          Inet Value
     * @param content     Input stream used for the content.
     */
    public ClientSystem(String destination, byte[] source, InetAddress ip, FileInputStream content) {

        this.destination = destination;
        this.source = source;
        this.ip = ip;
        this.content = content;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }


    /**
     * Initiate contact with the server, exchange file size, and once
     * that is done, send in the actual packets, and receive results.
     */
    public void start() {
        try {
            // Convert the data in bytes.
            ByteBuffer bb = ByteBuffer.allocate(8);
            address = ip.getHostAddress();
            //Get the value of the final chunk size.
            lastChunk = init(source);
            bb.putInt(source.length).putInt(lastChunk);
            byte[] size = bb.array();

            DatagramPacket packet
                    = new DatagramPacket(size, size.length, ip, 63001);
            System.out.println("started");

            socket.send(packet);
            //Send the file data
            ManageTransfer.sendingThread(source, socket, ip, 63001,false);
            seq = 0;
            receiveResults();
            content.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the last chunk
     *
     * @param source Source file
     * @return Last chunk size.
     */
    public static int init(byte[] source) {
        int len = source.length;
        int actualDivs = len / chunk;
        // Calculate what should be the chunk size for the final byte vals
        return len - chunk * actualDivs;

    }

    /**
     * Start the thread.
     *
     */
    @Override
    public void run() {
            start();
    }

    /**
     * Convert int into a byte array.
     *
     * @param input Target byte array.
     * @param data  source integer
     * @param start starting index in the array where the byte values
     *              are to be stored.
     */
    public static void intToBytes(byte[] input, int data, int start) {
        input[start] = (byte) ((data >> 24) & 0xff);
        input[start + 1] = (byte) ((data >> 16) & 0xff);
        input[start + 2] = (byte) ((data >> 8) & 0xff);
        input[start + 3] = (byte) ((data) & 0xff);


    }

    /**
     * Receive the results back from the server and print them.
     *
     */
    public static void receiveResults() {
        byte[] receive = new byte[65004];
        InetAddress dest;
        int port;
        DatagramPacket packet;

        System.out.println("Fetching results");
        while (true) {
            try {
                packet = new DatagramPacket(receive, receive.length);
                socket.receive(packet);
                dest = packet.getAddress();
                port = packet.getPort();
                seq = ServerSystem.getInt( receive, 0);
                //Store the incoming results in a file.
                FileOutputStream fos = new FileOutputStream(
                        "result1.txt", true);
                fos.write(receive, 4, seq);
                fos.close();
                packet = new DatagramPacket(ackD, ackD.length, dest, port);
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (seq!=65000)
                    break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader("result1.txt"));

        String line;

        //Read till there are no more lines left.
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

