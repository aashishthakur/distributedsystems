package edu.rit.cs;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

/**
 * Handles client side of the peer, responsible for connecting with
 * the miniserver, and updating status, connecting with other peers.
 *
 *
 * @author  Aashish Thakur
 *
 */
public class ClientRPC implements Runnable {
    public String anchorAddress;
    public KademliaPeer myPeer;
    public static int destination;
    public static String data;
    int mode = 0;
    static String address = "";
    static String port = "";
    static String method = "";

    /**
     * Setup for client
     *
     *
     * @param anchorAddress             Address of the anchor
     * @param myPeer                    Peer details
     * @param mode                      mode for thread
     */
    public ClientRPC(String anchorAddress, KademliaPeer myPeer, int mode) {
        this.anchorAddress = anchorAddress;
        this.myPeer = myPeer;
        this.mode = mode;

    }

    public void clientStart() {
        URL serverURL = null;

        try {

            // Once the client session object is created, you can use to send a series
            // of JSON-RPC 2.0 requests and notifications to it.

            // Sending an example "getTime" request:
            // Construct new request

            address = Config.SERVER_IP;
            port = Config.MINI_SERVER_PORT;


            method = "online";
//            int i
            while (true) {

                int requestID = 1;
                serverURL = new URL("http://" + address + ":" + port);
                // Create new JSON-RPC 2.0 client session
                JSONRPC2Session mySession = new JSONRPC2Session(serverURL);
                JSONRPC2Request request = new JSONRPC2Request(method, requestID);

                Map<String, Object> myParams = new HashMap<>();

                /*
                Select how to proceed, if no ip address is provided,
                contact the miniserver so as to update it of being online,
                Else, after that contact the ip address and add to the table.
                 */
                switch (method) {
                    case "contact":
                        myParams = new HashMap<>();
                        myParams.put("peer", myPeer);
                        myParams.put("anchor", true);
                        myParams.put("id", myPeer.getNodeID());
                        break;
                    case "online":
                        myParams = new HashMap<>();
                        myParams.put("ip", myPeer.getIpAddress());
                        myParams.put("peer", myPeer);
                        myParams.put("port", String.valueOf(myPeer.getPort()));
                        myParams.put("id", myPeer.getNodeID());
                        break;
                    case "wait":
                        //new thread
                        break;
                    case "findClosest":
                        //new thread
                        break;
                    case "addfile":
                        System.out.println("Sending to "+address);
                        myParams = new HashMap<>();
                        myParams.put("fileID",destination);
                        myParams.put("data",data);
                        break;


                }

                request.setNamedParams(myParams);
                // Send request
                JSONRPC2Response response = null;

                // Only do when we have an address.
                if (address!=null && !address.equals("")) {
                    try {
                        response = mySession.send(request);
//                    mySession.
                        switch (method) {
                            case "online":
                                if ((Boolean) response.getResult()) {
                                    System.out.printf("Node %d reported Online\n", myPeer.getNodeID());
                                    if (!anchorAddress.equals("")) {
                                        address = anchorAddress;
                                        port = Config.SERVER_PORT_VAL;
                                        method = "contact";
                                    } else {
                                        address = "";
                                        method = "wait";
                                    }
                                    // Start the refresh mode
                                    mode=1;
                                    Thread thread = new Thread(this);
                                    thread.start();
                                }
                                break;
                            case "contact":

                                HashMap<String, Object> peerResp = (HashMap<String, Object>) response.getResult();
                                KademliaPeer incomingPeer = new KademliaPeer();
                                incomingPeer.setNodeID(getIntFromLong(peerResp.get("nodeID")));
                                incomingPeer.setIpAddress((String) peerResp.get("ipAddress"));
                                incomingPeer.setPort((String) peerResp.get("port"));
                                System.out.println("Success");
                                // Add to the tree.
                                ServerRPC.connectNewNode(incomingPeer, false);
//                                port = incomingPeer.getPort();
                                method = "wait";
                                address = "";
                                break;
                            case "addfile":
                                method = "wait";
                                address = "";
                        }


                    } catch (JSONRPC2SessionException e) {

                        System.err.println("Failure to connect with the server, with URL: "+ serverURL.toString());
                        System.exit(1);
                    }

                    // Print response result / error
                    if (response.indicatesSuccess())
                        System.out.println(response.getResult());
                    else
                        System.out.println(response.getError().getMessage());
                }
            }

        } catch (MalformedURLException e) {
            // handle exception...
            e.printStackTrace();
//            System.err.println(e.printStackTrace());
        }
    }

    @Override
    public void run() {
//        System.out.println("Mode =="+ mode);
        if (mode==0)
        clientStart();
        else refresh();
    }

    /**
     * Refresh helps connect to all the nodes not immediately know,
     * by recursively finding out the closest point of contacts and
     * getting their information.
     */
    public void refresh() {
        System.out.println("refresh started!");
        while (true) {
            try {
                int node = new Random().nextInt(Config.total);
                if(node == myPeer.getNodeID() || routingTable.peerInfoHashMap.containsKey(node)){

                }
//                System.out.println("New target! "+ node);
                if (!routingTable.peerInfoHashMap.containsKey(node)) {
                    int requestID = 1;
                    URL serverURL = null;

                    serverURL = new URL("http://" + Config.SERVER_IP + ":" + Config.MINI_SERVER_PORT);
                    String method = "ispeeronline";
                    Map<String, Object> myParams = new HashMap<>();
                    myParams.put("id",node);
                    myParams.put("myid",myPeer.getNodeID());
                    myParams.put("myPort",myPeer.getPort());
                    myParams.put("myIp",myPeer.getIpAddress());

                    JSONRPC2Response response = formRequest(serverURL,method, myParams);
                    // ONly proceed when we have a response.
                    if (response!= null && (boolean)response.getResult()) {
                        PeerInfo resPeer = ServerRPC.rt.findClosest(node,new HashMap<>());
                        int i = 0;
//                        System.out.println("in: " + myPeer.getNodeID());
                        while (i<Config.total-2) {
                            if (resPeer!=null){
                                // ask for the closest node in the server.
                                method = "findClosest";
                                serverURL = new URL("http://" + resPeer.getIpAddress() + ":"
                                    + resPeer.getPort());
                                response = formRequest(serverURL,method, myParams);
                                if (response!=null && response.getResult()!=null){
                                HashMap<String, Object> peerInfo = (HashMap<String, Object>) response.getResult();
                                int resId = Integer.parseInt((String) peerInfo.get("id"));
                                myParams.put(String.valueOf(resId),String.valueOf(resId));
                            if (resId==myPeer.getNodeID()){
                                break;
                            }

                            KademliaPeer peer = new KademliaPeer();
                            // Add entry if it's not present and closer than existing ones.
                            if (!routingTable.peerInfoHashMap.containsKey(resId)){
                                peer.setNodeID(resId);
                                peer.setPort((String) peerInfo.get("port"));
                                peer.setIpAddress((String) peerInfo.get("ipAddress"));
                                ServerRPC.connectNewNode(peer, false);
                                resPeer = new PeerInfo(peer.getIpAddress(),peer.getPort(),
                                        (String) peerInfo.get("id"));
                                if (resId==node){
//                                    System.out.println("New node found in refresh!");
                                    break;
                                }
                            }else {
                                resPeer=routingTable.peerInfoHashMap.get(resId);
                            }


                        }else break;
                            i++;
                        }
                    }}
                }
                // Repeat after small intervals
                Thread.sleep(5000);
            } catch (MalformedURLException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Generic request handling method
     *
     * @param serverURL             Url to be pinged
     * @param method                Method to be used.
     * @param myParams              Parameters for the request.
     * @return                      Response of the request.
     */
    public static JSONRPC2Response formRequest(URL serverURL, String method, Map<String, Object> myParams) {
        // Create new JSON-RPC 2.0 client session

        int requestID = 1;
        JSONRPC2Session mySession = new JSONRPC2Session(serverURL);
        JSONRPC2Request request = new JSONRPC2Request(method, requestID);
        request.setNamedParams(myParams);
        // Send request
        JSONRPC2Response response = null;

        try {
            response = mySession.send(request);

        } catch (JSONRPC2SessionException e) {

            System.err.println(e.getMessage());
            // handle exception...

        }

        if (response!=null && method.equals("addfile")) {
            if (response.indicatesSuccess())
                System.out.println(response.getResult());
            else
                System.out.println(response.getError().getMessage());
        }

            return response;
    }

    public static int getIntFromLong(Object num) {
        return Integer.parseInt(String.valueOf(num));
    }
}
