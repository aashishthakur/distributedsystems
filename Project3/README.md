<h1> Kademlia distributed hash table</h1>
<h2>Components </h2>
<h3>Client</h3>
<p>Client uploads a file onto the server and in return gets a hash value, this hash value can later be used for retrieval.<p>

<h3>Mini server</h3>
<p>Mini server acts as the front end to the client by taking the files and retrieving it when needed, it also plays the role of helping the anchor node by providing it information about hash and files which have to be stored. It also does a mod operation on the hash so as to limit it within the constraints of the nodes available.</p>

<h3>Anchor node</h3>
<p>Keeps track of active nodes, in case a node goes down gracefully, it's data is taken and given to the nearest node.
Also, sets up its own routing table w.r.t Kademlia routing tree structure, then it does prefix matching to find the closest match to the target node, and this lookup continues to happen till a match is found. To increase the speed caching is done but only of a limited space, this is maintained using an LRU cache.</p>

<h3>Nodes</h3>
<p>Responsible for storing the client's file, also in case a lookup is requested they return the nodes present in their table, with information such as their IP addresses, Port Numbers and Node ids. First step to this also involves making their own routing tables. When the nodes start they get Anchor node details from the mini server, they provide their online status. And when requested if they have the file, they return the file.</p>


<h3>Instructions to run<h3>
Start & end
==============
sudo docker-compose -f docker-compose-p3.yml -p Project3 up -d --build --scale  miniserver=1 --scale peer=2
sudo docker-compose -f docker-compose-p3.yml -p Project3 down

ATTACH
==============
sudo docker exec -it project3_miniserver_1 bash
java -cp target/Project3-1.0-SNAPSHOT.jar edu.rit.cs.MiniServer 60051

sudo docker exec -it project3_peer_1 bash
java -cp target/Project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaPeer p 2
java -cp target/Project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaPeer p 4 170.11.0.6

java -cp target/Project3-1.0-SNAPSHOT.jar edu.rit.cs.KademliaPeer c 170.11.0.6

sudo docker exec -it project2_publisher_2 bash
    ./start.sh 2

sudo docker exec -it project2_subscriber_1 bash

sudo docker exec -it project2_subscriber_2 bash