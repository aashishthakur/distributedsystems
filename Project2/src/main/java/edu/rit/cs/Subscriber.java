package edu.rit.cs;

public interface Subscriber {
	/*
	 * subscribe to a topic
	 */
	public void subscribe(Topic topic);
	

	/*
	 * unsubscribe from a topic 
	 */
	public void unsubscribe(Topic topic);
	
	/*
	 * unsubscribe to all subscribed topics
	 */

	public void unsubscribe();
	
	/*
	 * show the list of topics current subscribed to 
	 */
	public void listSubscribedTopics();
	
}
