package edu.rit.cs;

public class Config {
//    public static final String SERVER_IP = "170.10.0.20";
    public static final String SERVER_IP = "170.11.0.20";
    public static final String MINI_SERVER_PORT = "60051";
    public static final String SERVER_PORT_VAL = "65001";
    public static final int entries = 4;
    public static final int total = 16;

//    public static final String DEFAULT_PEER_IP = "127.0.0.1";
//    public static final String DEFAULT_PORT_PREFIX = "8080";
}
