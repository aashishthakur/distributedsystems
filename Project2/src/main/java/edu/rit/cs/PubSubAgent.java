package edu.rit.cs;

/**
 * Handles both bulisher and subscriber
 *
 */

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class PubSubAgent implements Publisher, Subscriber, Runnable {

    private String ip;
    private static ObjectOutputStream outputStream;
    private static ObjectInputStream inputStream;
    private static ObjectInputStream mapInputStream;
    private Socket mySocket;
    private static HashMap<String, Topic>  subscribedTopics = new HashMap<>();
    private int newServerPort;
    public String userID;

    public enum pubOptions {PUBLISH, ADVERTISE}
    public enum subOptions {SUBSCRIBE, UNSUBSCRIBE, UNSUBSCRIBEALL , SUBLIST, EXIT}

    public static HashMap<Integer, Topic> topicMap = new
            HashMap<>();

    static String type;


    /**
     *
     * Starts the initial connection and multi-threading.
     *
     * @param ip
     */
    public PubSubAgent(String ip) {
        this.ip = ip;
        Thread listner = new Thread(this);
        try {
            startConnection(9092);
            newServerPort = (int) inputStream.readObject();

            System.out.println(newServerPort);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            endConnection();
        }
        listner.start();


    }


    /**
     * UI to communicate with the manager
     * and verify the credentials
     *
     *
     */
    public void verifyCredentials() {
        try {
            Scanner sc = new Scanner(System.in);
            startConnection(newServerPort);
            while (true) {
                System.out.println("Please enter user id");

                userID = sc.next();
                System.out.println("Please enter password");
                String password = sc.next();
                System.out.println("Please enter 'p' for publisher or 's' for subscriber");
                type = sc.next();
                // 01: Login verification,
                type = type.toLowerCase();
                if (!(type.equals("p") || type.equals("s"))) {
                    System.err.println("invalid type");
                    continue;
                }
                String creds[] = {userID, password, type};
                outputStream.writeObject(1);
                outputStream.writeObject(creds);
                if ((boolean) inputStream.readObject()) {

                    System.out.println("Login successful");
                    endConnection();
                    break;
                } else {
                    System.err.println("Login failure");
                    System.exit(1);

                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    /**
     * Starts the connection
     *
     *
     * @param port
     */
    public void startConnection(int port) {
        try {
            mySocket = new Socket(ip, port);
            outputStream = new ObjectOutputStream(mySocket.getOutputStream());
            inputStream = new ObjectInputStream(mySocket.getInputStream());
        } catch (IOException e) {
            System.out.println("Connection issue");
            System.exit(1);
        }


    }

    /**
     * Ends a connection
     *
     *
     */
    public void endConnection() {

        try {
            inputStream.close();
            outputStream.close();
            mySocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Subscribe to a given topic
     *
     *
     * @param topic         Target topic.
     */
    @Override
    public void subscribe(Topic topic) {
        // TODO Auto-generated method stub
        try {
            if (!subscribedTopics.isEmpty() && subscribedTopics.containsKey(topic.getId())){
                System.out.println("Already subscribed to topic"+ topic );
            }else{
                startConnection(newServerPort);
                outputStream.writeObject(5);
                outputStream.writeObject(userID);
                outputStream.writeObject(topic);
            }

        }catch (IOException e){
            e.printStackTrace();
        }finally {
            endConnection();
        }

    }


    @Override
    public void unsubscribe(Topic topic) {
        try {
            startConnection(newServerPort);
            outputStream.writeObject(6);
            outputStream.writeObject(userID);
            outputStream.writeObject(topic);
            System.out.println("No longer subcscribed to "+ topic);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            endConnection();
        }

        // TODO Auto-generated method stub
    }

    @Override
    public void unsubscribe() {
        // TODO Auto-generated method stub
        try {
            startConnection(newServerPort);
            outputStream.writeObject(7);
            outputStream.writeObject(userID);
            System.out.println("User "+ userID +" no longer subscribed to anything");
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            endConnection();
        }

    }

    @Override
    public void listSubscribedTopics() {
        // TODO Auto-generated method stub
        try {
            startConnection(newServerPort);
            outputStream.writeObject(8);
            outputStream.writeObject(userID);
            subscribedTopics = (HashMap<String, Topic>) inputStream.readObject();
            printSubscribedTopics();
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            endConnection();
        }

    }



    @Override
    public void publish(Event event) {
        // TODO Auto-generated method stub
        try {
            startConnection(newServerPort);
            System.out.println("Publishing...");
            outputStream.writeObject(3);
            outputStream.writeObject(event);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            endConnection();
        }


    }

    @Override
    public void advertise(Topic newTopic) {
        // TODO Auto-generated method stub
        try {
            startConnection(newServerPort);
            outputStream.writeObject(4);
            outputStream.writeObject(userID);
            outputStream.writeObject(newTopic);
            System.out.println(inputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            endConnection();
        }

    }


    @Override
    public void run() {
        listner();

    }


    /**
     *
     * Listen for incoming data.
     *
     */
    public void listner() {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(59000);
            Socket clientSocket;
            while (true) {
                clientSocket = serverSocket.accept();
                System.out.println("Got something");
                outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
                inputStream = new ObjectInputStream(clientSocket.getInputStream());

                int type = (int) inputStream.readObject();
                switch (type){
                    case 1:
                        Topic topic = (Topic) inputStream.readObject();
                        System.out.println("New Topic:\n " + topic);
                        break;
                    case 2:
                        Event event = (Event) inputStream.readObject();
                        System.out.println("Update for subscription for topic: " + event.getTopic());
                        System.out.println("Title: " + event.getTitle());
                        System.out.println("Content:" + event.getContent());
                        break;
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Main started
     *
     * @param args  main arguments
     */
    public static void mainSS(String[] args) {

        if (args.length == 0) {
            System.err.println("Invalid Inputs, please pass the server address");
            System.exit(1);

        }

        String server_address = args[1];


        PubSubAgent pubSub = new PubSubAgent(server_address);
        pubSub.verifyCredentials();

        if (type.equals("s")) {
            subcriberGUI(pubSub);
        } else publisherGUI(pubSub);

    }


    /**
     * Give the subcriber options like subscribe, unsubscriber, etc.
     *
     *
     * @param pubSub            Object of pub sub
     */
    public static void subcriberGUI(PubSubAgent pubSub) {

        while (true) {
            int i = 1;
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter");
            for (subOptions sub : subOptions.values()) {

                System.out.printf(" %d for %s\n", i, sub);
                i++;
            }
            int index = sc.nextInt();

            switch (subOptions.values()[index - 1]) {
                case SUBSCRIBE:
                    int choice = pubSub.topicSelection(sc);
                    if (topicMap.containsKey(choice)) {
                        pubSub.subscribe(topicMap.get(choice));
                    } else {
                        System.out.println("Invalid choice");
                    }
                    break;
                case UNSUBSCRIBE:
                    choice = pubSub.topicSelection(sc);
                    if (topicMap.containsKey(choice)) {
                        pubSub.unsubscribe(topicMap.get(choice));
                    } else {
                        System.out.println("Invalid choice");
                    }
                    break;
                case SUBLIST:
                    pubSub.listSubscribedTopics();
                    break;
                case UNSUBSCRIBEALL:
                    pubSub.unsubscribe();
                    break;
                case EXIT:
                    break;

            }
        }
    }


    /**
     *
     * Publisher options are displayed here.
     *
     *
     * @param pubSub
     */
    public static void publisherGUI(PubSubAgent pubSub) {
        while (true) {
            int i = 1;
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter");
            for (pubOptions pub : pubOptions.values()) {

                System.out.printf(" %d for %s\n", i, pub);
                i++;
            }
            int index = sc.nextInt();
            if (index == 0) {
                System.err.println("Invalid choice");
                continue;
            }
            switch (pubOptions.values()[index - 1]) {
                case PUBLISH:
                    System.out.println("Please select a topic:\n");
                    int choice = pubSub.topicSelection(sc);
                    Event e;
                    if (topicMap.containsKey(choice)) {
                        System.out.println("Enter Title");
                        String title = sc.next();
                        System.out.println("Enter Content");
                        String content = sc.next();
                        e = new Event();
                        e.setTitle(title);
                        e.setTopic(topicMap.get(choice));
                        e.setContent(content);
                        pubSub.publish(e);
                    } else
                        System.out.println("Invalid choice");
                    break;

                case ADVERTISE:
                    while (true) {
                        System.out.println("Enter Topic name");
                        String topicName = sc.next();
                        pubSub.fetchTopics();
                        if (!topicMap.containsValue(topicName)) {
                            Topic t = new Topic();
                            t.setName(topicName);
                            pubSub.advertise(t);
                            break;
                        }else {
                            System.out.println("Topic exists");
                        }
                    }
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;

            }
        }
    }


    /**
     * fetch the available topics list.
     *
     *
     */
    public void fetchTopics() {
        try {
            startConnection(newServerPort);
            outputStream.writeObject(2);
            mapInputStream = new ObjectInputStream(inputStream);
//            topicMap = (ArrayList<Topic>) mapInputStream.readObject();
            topicMap = (HashMap<Integer, Topic>) mapInputStream.readObject();
            mapInputStream.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            endConnection();
        }
    }

    /**
     * Print the available topics list.
     *
     *
     */
    public void printTopics() {

        if (topicMap.isEmpty()) {
            System.out.println("No topics available right now.");
        } else {
            System.out.println("List of available topics:");
            for (int topicID : topicMap.keySet()) {
                System.out.printf("%d : %s\n", topicID, topicMap.get(topicID));
            }
        }

    }

    /**
     *
     * Print all the subscribed topics.
     *
     *
     */
    public void printSubscribedTopics() {

        if (subscribedTopics.isEmpty()) {
            System.out.println("No subscriptions");
        } else {
            System.out.println("List of available topics:");

            for (String topicName : subscribedTopics.keySet()) {
                System.out.printf("%d : %s\n", subscribedTopics.get(topicName).getId(), topicName);
            }
        }
    }

    /**
     * Selection of a topic form a list of topics.
     *
     *
     *
     * @param sc            User input
     * @return              return what the user has selected.
     */
    public int topicSelection(Scanner sc){

        fetchTopics();
        printTopics();
        return sc.nextInt();

    }


}
