package edu.rit.cs;

import java.io.Serializable;

/**
 * Event management
 *
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class Event implements Serializable {
	private int id;
	private String title;
	private String content;
	private Topic topic;
	public int type;



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Topic getTopic() {
		return topic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
