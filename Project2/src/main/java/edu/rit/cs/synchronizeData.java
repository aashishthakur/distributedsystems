package edu.rit.cs;

import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * This class helps with keeping all the data synchronized and
 * makes changes to the tables being used.
 *
 *
 * @author Aashish Thakur(at1948@rit.edu)
 * @version 1.0
 */
public class synchronizeData {

    public static HashMap<Integer,Topic> topicMap = new
            HashMap<>();

    static int topicID = 1;



    public static HashMap<String,subscribeAttributes> subscriberToProperties = new
            HashMap<>();
    public static HashMap<String,publisherAttributes> publisherToProperties = new
            HashMap<>();
    public static HashMap<String,String> UserIdToPassword = new
            HashMap<>();
    public Set<String> offlineSubs = new HashSet<>();

    public static HashMap<String,ArrayList<String>> topicToSubscriber = new
            HashMap<>();
    public static ArrayList<Event> eventsPending = new ArrayList<>();
    public static ArrayList<Topic> topicsToBeAdvertised = new ArrayList<>();

    public static ArrayList<Event> getEventsPending() {
        return eventsPending;
    }

    public static ArrayList<Topic> getTopicToBeAdvertised() {
        return topicsToBeAdvertised;
    }


    public static HashMap<String, publisherAttributes> getPublisherToProperties() {
        return publisherToProperties;
    }

    public static HashMap<String, subscribeAttributes> getSubscriberToProperties() {
        return subscriberToProperties;
    }



    /**
     * Verify the user, if they exist check if they have anything pending,
     * else register new users.
     *
     * @param creds
     * @param clientSocket
     * @return
     */
    public boolean verifySession(String[] creds, Socket clientSocket) {
        // Get the credentials
        String userID = creds[0];
        String pass = creds[1];
        String type = creds[2];
        //Check if the user id in the table, if not then register, else verify.
        if (UserIdToPassword.isEmpty() || !UserIdToPassword.containsKey(userID)) {
            UserIdToPassword.put(userID, pass);

            int portNum = clientSocket.getLocalPort();
            InetAddress ia = clientSocket.getInetAddress();
            String ip = ia.getHostAddress().trim();

            // New users get updated in the respective tables along with their
            // properties.
            if (type.equals("s")) {
                subscribeAttributes s = new subscribeAttributes(portNum, ip, userID);

                s.turnOn();
                subscriberToProperties.put(userID, s);
            } else {
                publisherAttributes p = new publisherAttributes(portNum, ip);
                publisherToProperties.put(userID, p);
            }
            return true;
        } else {

            boolean equals = UserIdToPassword.get(userID).equals(pass);
            subscribeAttributes info = subscriberToProperties.get(userID);

                if (equals) {
                    if (type.equals("s")) {
                        info.turnOn();
                        if (info.pendingTopics.size() > 0 ||
                                info.pendingEvents.size() > 0) {

                            offlineSubs.add(userID);
                        }
                    }
                }
                return equals;

        }
    }



    /**
     * Unsubscribe user from a topic
     *
     *
     * @param t                     Topic to be unsubscribed from
     * @param subscriber            Subscriber who's unsubscribing
     * @return                      True if successful
     */
    public boolean unsubscribeFromTopic(Topic t, String subscriber){
        if (!topicToSubscriber.isEmpty()) {
            ArrayList<String> subList = topicToSubscriber.get(t.getName());
            System.out.println("Length before "+subList.size());
            for (int i = 0; i < subList.size();i++){
                if (subList.get(i).equals(subscriber)){
                    //Add topic to the list of subscribed topic for the subscriber.
                    System.out.println(i);
                    subscriberToProperties.get(subList.remove(i)).removeSubscriber(t);
                    System.out.println("Length now+ "+subList.size());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return the subscriptions for a particular subscriber.
     *
     *
     * @param subscriber            Subscriber who's subscriptions are needed
     * @return                      List of topics
     */
    public HashMap<String, Topic> returnSubscribedTopic(String subscriber){
        HashMap<String, Topic> result = new HashMap<>();
        if (!subscriberToProperties.isEmpty()&&
                subscriberToProperties.containsKey(subscriber)){
            result = subscriberToProperties.get(subscriber).getSubscribedTo();
        }
        return result;
    }


    /**
     * Subscribe the client to the topic.
     *
     * @param t                     Topic for which subscription has to be made
     * @param subscriber            Subscriber who wants to subscribe
     */
    public void subscribetoTopic(Topic t, String subscriber){
        if (!topicToSubscriber.isEmpty() && topicToSubscriber.containsKey(t.getName())){
            topicToSubscriber.get(t.getName()).add(subscriber);

        }else {
            ArrayList<String> subList = new ArrayList<>();
            subList.add(subscriber);
            topicToSubscriber.put(t.getName(),subList);
        }

        //Add the topic to the subscriber.
        subscriberToProperties.get(subscriber).addSubscriber(t);
    }

    /**
     * Unsubscribe from all topics for a user.
     *
     *
     * @param subscriber              subscriber for whom
     */
    public void unsubscribeFromAllTopics(String subscriber){
        int j;
        HashMap<String, Topic> subs = subscriberToProperties.get(subscriber).getSubscribedTo();

        for (Topic t:subs.values()){
            if (topicToSubscriber.containsKey(t.getName())) {
                ArrayList<String> subNames = topicToSubscriber.get(t.getName());
                for (j = 0; j<subNames.size();j++){
                    if (subNames.get(j).equals(subscriber)){
                        System.out.println(topicToSubscriber.get(t.getName()).size()+" Size before");

                        subNames.remove(j);
                        System.out.println(topicToSubscriber.get(t.getName()).size()+" Size after");
                        break;
                    }
                }
            }else{
                System.err.println("Table mismatch");
            }
        }
        subs.clear();
    }

    /**
     * Get the list of all the IP addresses for a  topics subscription
     *
     * @param tName                 Name of the topic
     * @return                      List of subscriber details.
     */
    public ArrayList<subscribeAttributes> getSubcribersIPForTopic(String tName){
        ArrayList<subscribeAttributes> result = new ArrayList<>();
        // Loop over all the subscribers.
        if (!topicToSubscriber.isEmpty() && topicToSubscriber.containsKey(tName)){
            for (String sub:topicToSubscriber.get(tName)){
                result.add(subscriberToProperties.get(sub));
            }

        }
        return result;

    }

    /**
     *
     * Add to the list of pending events.
     *
     * @param inactiveSubEvents         Mapping of subscribers and their pending events.
     */
    public void pendingEvents(HashMap<String, ArrayList<Event>> inactiveSubEvents) {
        for (String subID: inactiveSubEvents.keySet()
        ) {
            subscriberToProperties.get(subID).turnOff();
            subscriberToProperties.get(subID).addEvents(inactiveSubEvents.get(subID));
        }
    }

    /**
     * Add to the list of pending topics.
     *
     *
     * @param inactiveSubTopic      Mapping of subscribers and their pending topics.
     */
    public void pendingTopics(HashMap<String, ArrayList<Topic>> inactiveSubTopic) {
        for (String subID: inactiveSubTopic.keySet()
        ) {
            subscriberToProperties.get(subID).turnOff();
            subscriberToProperties.get(subID).addTopics(inactiveSubTopic.get(subID));
        }

    }


}
