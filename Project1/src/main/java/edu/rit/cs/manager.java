package edu.rit.cs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Manager program handles both client and server, which start depending on the number
 * of arguments.
 *
 * @author Aashish Thakur
 * @since 02/04/2020
 */
public class manager {
    //Contains the actual file
    static byte[] source;

    /**
     * The main program
     *
     * @param args Arguments could either be  source file location and target IP
     *             or destination file location.
     */
    public static void main(String[] args) {
        //Client
        if (args.length == 2) {
            FileInputStream content;
            String fname = args[0];
            try {
                //Destination
                InetAddress ip = InetAddress.getByName(args[1]);
                //Create a new file input stream.
                content = new FileInputStream(new File(fname));
                source = content.readAllBytes();
                //Start the client thread.
                Thread client = new Thread(new ClientSystem(args[1], source, ip, content));
                client.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // Server
        else if (args.length == 0){
            // Start the server thread and pass in the type and file destination loc.
            Thread server = new Thread(new ServerSystem("affr2.csv"));
            server.start();
        }else {
            System.err.println("Faulty parameters");
        }
    }


}
