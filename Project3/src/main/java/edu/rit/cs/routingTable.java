package edu.rit.cs;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * manages the whole routing table and its functions like insert,
 * find closest, delete.
 *
 *
 */
public class routingTable {

    public KademliaPeer peer;
    public static ConcurrentHashMap<Integer,PeerInfo> peerInfoHashMap = new ConcurrentHashMap<>();

    // Start building the tree when s node is formed.
    public routingTable(KademliaPeer peer) {
        this.peer = peer;
        tree.startTree(peer);
    }

    // Insertion as explained in earlier files
    public  boolean insert(KademliaPeer newPeer){
        int nodeNum = newPeer.getNodeID();
        if (peerInfoHashMap.containsKey(nodeNum)){
            System.out.printf("Key %d already present",nodeNum);
            return false;
        }else if (!peerInfoHashMap.containsKey(nodeNum)){
            if (tree.setNode(nodeNum)){
                peerInfoHashMap.put(nodeNum,new PeerInfo(newPeer.getIpAddress(),String.valueOf(newPeer.getPort())
                , String.valueOf(nodeNum)));
                peerInfoHashMap.remove(tree.oldNode);
                tree.setOldNode(-1);
            }else return false;
        }
        return true;
    }


    // Finds the closest match for a given target.
    public PeerInfo findClosest(int targetNode, Map<String, Object> visited) {
        int min = Integer.MAX_VALUE;
        if (peerInfoHashMap.containsKey(targetNode)){
            if  (!visited.isEmpty() && !visited.containsKey(String.valueOf(targetNode)))
            return peerInfoHashMap.get(targetNode);
        }
        for (int pi:
                peerInfoHashMap.keySet()) {
                    if (pi!=peer.getNodeID() ){
                        if  (!visited.isEmpty() && visited.containsKey(String.valueOf(pi)))
                            continue;
                    min = tree.xorComp(min,pi,targetNode);
                    }


        }
        if (min == Integer.MAX_VALUE){
            return null;
        }
//        System.out.println("The min value for peer: "+ peer.getNodeID()+ " is "+ min);
        return peerInfoHashMap.get(min);
    }



}

