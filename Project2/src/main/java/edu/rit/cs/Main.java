package edu.rit.cs;

/**
 * Main class to just start event manager, pub and sub.
 * Pass 1 for manager, 2 for pub/sub.
 */
class Main {
    public static void main(String args[]) {
        if (args.length > 0) {
            // Get the node number (first arg)
            int nodeNum = Integer.parseInt(args[0]);
            System.out.println("I'm choice " + nodeNum);

            try {
                if (nodeNum == 1) {
                    EventManager.mainSS(args);
                } else if (nodeNum==2){
                    PubSubAgent.mainSS(args);
                }
                while (true) {
                    Thread.sleep(1000);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else
            System.out.println("No input args! Must specify choice");
    }
}
