package edu.rit.cs;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

class ManageTransfer {

    /***
     *
     * Send data from one system to another on a specified port.
     *
     * @param source                Bytes to be sent
     * @param socket                Socket on which the bytes are sent
     * @param ip                    IP address of the destination.
     */
    static void sendingThread(byte[] source, DatagramSocket socket, InetAddress ip, int port,boolean mode) {

        byte[] ackD = new byte[1];
        int chunk = 65000;
        int lastChunk = ClientSystem.init(source);
        int seq = 0;
        //how much data is being sent over the network
        byte[] send = new byte[65004];

        while (seq < source.length) {
            int range = (seq == source.length - lastChunk ? lastChunk : chunk);
            if (mode){

                ClientSystem.intToBytes(send, range, 0);
            }else
            ClientSystem.intToBytes(send, seq, 0);
            for (int i = 4; i <= range + 3; i++) {
                send[i] = source[seq];
                seq++;
            }

            // send the packets.
            DatagramPacket packet = new DatagramPacket(send, send.length, ip, port);

            try {
                socket.send(packet);
                packet = new DatagramPacket(ackD, ackD.length);
                socket.receive(packet);

            } catch (IOException e) {
                e.printStackTrace();
            }
            send = new byte[65004];
        }

        System.out.println("Reviews sent successfully ");
    }
}
